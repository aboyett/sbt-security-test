import sbt._
import sbt.Keys._
import play.Play.autoImport._
import PlayKeys._
import AddSettings._
import play.PlayScala
	

lazy val root = (project in file(".")).enablePlugins(PlayScala)

name := "sbt-security-test"

version := "0.0.1"

scalaVersion := "2.10.5"

libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-actor" % "2.3.4",
    "com.typesafe.akka" %% "akka-testkit" % "2.3.4",
    "com.typesafe.akka" %% "akka-dataflow" % "2.3.4",
    "com.typesafe.akka" % "akka-durable-mailboxes" % "2.1.1",
    "com.amazonaws" % "aws-java-sdk" % "1.9.4" excludeAll (ExclusionRule(name = "httpcomponents")),
    "com.gilt" %% "jerkson" % "0.6.8-2",
    "net.liftweb" % "lift-json-ext_2.10" % "2.5.1",
    "net.liftweb" %% "lift-json" % "2.5.1",
    "net.liftweb" %% "lift-mongodb" % "2.5.1",
    "org.mongodb" % "casbah_2.10" % "2.8.1",
    "org.mongodb" % "casbah-core_2.10" % "2.8.1",
    "org.mongodb" % "casbah-commons_2.10" % "2.8.1",
    "org.scalatest" % "scalatest_2.10" % "1.9.1" % "test",
    "org.slf4j"            %  "log4j-over-slf4j"   % "1.7.5",
    "com.github.sdb" %% "play2-metrics" % "0.1.0",
    "org.scalaj" %% "scalaj-http" % "0.3.14" excludeAll (ExclusionRule(name = "junit")),
    "org.apache.curator" % "curator-x-discovery" % "2.6.0",
    "org.apache.curator" % "curator-framework" % "2.6.0",
    "org.apache.curator" % "curator-recipes" % "2.6.0",
    "net.databinder.dispatch" %% "dispatch-core" % "0.11.2",
    "com.codahale.metrics" % "metrics-core" % "3.0.0",
    "nl.grons" %% "metrics-scala" % "3.0.0",
    ws
)  
